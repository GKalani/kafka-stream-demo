package com.kgk.kafkastreamdemo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentDataKey {
    private Integer id;
    private String name;
}
