package com.kgk.kafkastreamdemo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentKafkaRecordKey {
    private String operation;
    private String table;
    private StudentDataKey data; //can't we use entity obj?
}
