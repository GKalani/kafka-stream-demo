package com.kgk.kafkastreamdemo.topology;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.time.Duration;

//@Component
public class WordCountProcessor {

    private static final Logger logger = LoggerFactory.getLogger(WordCountProcessor.class);
    private static final Serde<String> STRING_SERDE = Serdes.String();
    private static final Serde<Long> LONG_SERDE = Serdes.Long();

    @Value("${kafka.wordcount.topic}")
    private String topicName;

    @Autowired
    void buildPipeline(StreamsBuilder streamsBuilder){
        //StreamsBuilder streamsBuilder = new StreamsBuilder();
        logger.info("Word Count Processor");
        KStream<String, String> messageStream = streamsBuilder.stream(topicName, Consumed.with(STRING_SERDE, STRING_SERDE));

/*        messageStream.peek((k, v) -> logger.info("Ingested1: key => {}, val => {}", k, v))
                .flatMapValues((readOnlyKey, value) -> Arrays.asList(value.split(" ")))
                .peek((k, v) -> logger.info("Ingested2: key => {}, val => {}", k, v))
                .groupBy((key,value) -> value)
                .count(Materialized.with(STRING_SERDE, LONG_SERDE))
                .toStream()
                .peek((k, v) -> logger.info("Ingested3: key => {}, val => {}", k, v));*/

        messageStream
                .peek((k, v) -> logger.info("Ingested2: key => {}, val => {}", k, v))
                .groupBy((key,value) -> value)
                .windowedBy(TimeWindows.ofSizeWithNoGrace( Duration.ofSeconds(5)))
                .count(Materialized.with(STRING_SERDE, LONG_SERDE))
                .suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded()))
                .toStream()
                .peek((k, v) -> logger.info("Ingested within 5 sec: key => {}, val => {}", k, v));

    }

}
