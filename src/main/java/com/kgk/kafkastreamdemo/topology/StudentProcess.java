package com.kgk.kafkastreamdemo.topology;

import com.kgk.kafkastreamdemo.dto.StudentKafkaRecordKey;
import com.kgk.kafkastreamdemo.service.StudentService;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.apache.kafka.common.serialization.Serdes.Integer;
import static org.apache.kafka.common.serialization.Serdes.Long;
import static org.apache.kafka.common.serialization.Serdes.String;
import static org.apache.kafka.common.serialization.Serdes.*;

import java.lang.Long;
import java.lang.String;
import java.lang.Integer;

@Component
public class StudentProcess {

    private static final Logger logger = LoggerFactory.getLogger(StudentProcess.class);
    private static final Serde<String> STRING_SERDE = String();
    private static final Serde<Long> LONG_SERDE = Long();
    private static final Serde<Integer> INTEGER_SERDE = Integer();
    Serde<StudentKafkaRecordKey> studentKafkaRecordKeyJsonSerde = new JsonSerde<>(StudentKafkaRecordKey.class);
    Serde<List<Integer>> LIST_SERDE = ListSerde(ArrayList.class, Integer());
    @Autowired
    private StudentService studentService;

    @Value("${kafka.student.topic}")
    private String topicName;

    /**
     * 1. consume students details
     * 2. based on std_id, fetch student data from db
     * @param streamsBuilder
     */
    @Autowired
    void buildPipeline(StreamsBuilder streamsBuilder){
        logger.info("Student Processor");
        //StreamsBuilder streamsBuilder = new StreamsBuilder();

        KStream<String, StudentKafkaRecordKey> messageStream = streamsBuilder
                .stream(topicName, Consumed.with(STRING_SERDE, studentKafkaRecordKeyJsonSerde))
                .peek((k, v) -> logger.info("1. Ingested: key => {}, val => {}", k, v))
                .map((k, v) -> new KeyValue<>(v.getData().getId().toString(), v))
                .peek((k, v) -> logger.info("2. Reformatted Data : key => {}, Operation => {}, Data => {}", k, v.getOperation(), v.getData().getName()));

        KStream<String, Integer> latestRecordStream = messageStream
                .groupByKey(Grouped.with(STRING_SERDE, studentKafkaRecordKeyJsonSerde))
                .windowedBy(TimeWindows.ofSizeWithNoGrace( Duration.ofSeconds(30)))
                .reduce((value1, value2) -> value2, Materialized.with(STRING_SERDE, studentKafkaRecordKeyJsonSerde))
                .suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded()))
                .toStream()
                .peek((k, v) -> logger.info("3. Data after removing duplicates: key => {}, val => Operation => {} Data => {}", k, v.getOperation(), v.getData().getId()))
                .map((k,v) -> {
                    if(v.getOperation().equals("CREATE") || v.getOperation().equals("UPDATE")){
                        return new KeyValue<>("UPSERT", v.getData().getId());
                    }else {
                        return new KeyValue<>("DELETE", v.getData().getId());
                    }
                });

        latestRecordStream
                .peek((k, v) -> logger.info("4. UPSER/DELETE rec: key => {}, Data => {}", k, v))
                .groupByKey(Grouped.with(STRING_SERDE, INTEGER_SERDE))
                .windowedBy(TimeWindows.ofSizeWithNoGrace( Duration.ofSeconds(30)))
                .aggregate(ArrayList::new,
                        (key, value, aggregate) -> {
                            aggregate.add(value);
                            return aggregate;
                            }, Materialized.with(STRING_SERDE, LIST_SERDE))
                .suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded()))
                .toStream()
                .peek((k, v) -> logger.info("5. CREATE/UPDATE : key => {}, Operation => {}", k.key(), v))
                //.mapValues(new StudentValueMapper(studentService))
        ;


        /*
        * Message format
        * {
               "operation":"INSERT",
               "data":{
                  "id":1,
                  "name":"Jhon"
               },
               "table":"student"
            }
        * */

    }


   /* KStream<String, FtpSchedule> messageStream = streamsBuilder
            .stream(tripKeyTopic, Consumed.with(STRING_SERDE, STRING_SERDE))
            .peek((k,v) -> logger.info("Ingested: key => {}, val => {}", k, v))
            .mapValues(new TripKeyRowObjectMapper())
            .filter((k,v) -> v != null)
//                .mapValues(v ->  ((TripKeyRow)v).getBody().getTripPlanNumber())
            .mapValues(new TripRowMapper(ftpScheduleRepository))
            .filter((k,v) -> v != null);*/
}
