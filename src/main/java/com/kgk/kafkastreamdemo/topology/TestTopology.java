package com.kgk.kafkastreamdemo.topology;

import com.kgk.kafkastreamdemo.dto.StudentKafkaRecordKey;
import com.kgk.kafkastreamdemo.service.StudentService;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.util.Collections;

//@Component
public class TestTopology {

    private static final Logger logger = LoggerFactory.getLogger(WordCountProcessor.class);
    Serde<StudentKafkaRecordKey> studentKafkaRecordKeyJsonSerde = new JsonSerde<>(StudentKafkaRecordKey.class);
    @Autowired
    private StudentService studentService;

    @Value("${kafka.student.topic}")
    private String topicName;

    @Autowired
    void buildPipeline(StreamsBuilder streamsBuilder){
        logger.info("TestTopology");
        //studentService.getAllStudents();
        studentService.getStudentList(Collections.singletonList(2));

        KStream<String, StudentKafkaRecordKey> messageStream = streamsBuilder
                .stream(topicName, Consumed.with(Serdes.String(), studentKafkaRecordKeyJsonSerde))
                .peek((k, v) -> logger.info("1. Ingested: key => {}, val => {}", k, v));
    }
}
