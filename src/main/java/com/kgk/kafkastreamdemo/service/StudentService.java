package com.kgk.kafkastreamdemo.service;

import com.kgk.kafkastreamdemo.entity.Student;
import com.kgk.kafkastreamdemo.repository.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    private static final Logger logger = LoggerFactory.getLogger(StudentService.class);
    @Autowired
    private StudentRepository studentRepository;

    public List<Student> getAllStudents(){
        System.out.println("****** Student Service");
        logger.info("---- Student Service logs");
        List<Student>studentList = new ArrayList<>();
        studentRepository.findAll().forEach(studentList::add);
        for(Student s : studentList) {
            System.out.println(s.getId() + " - " +s.getName());
        }
        return  studentList;
    }

    public List<Student> getStudentList(List<Integer> idList){
        logger.info("---- Student Service logs: getStudentList()");
        List<Student>studentList = new ArrayList<>();

        //studentList = studentRepository.findAll();
        //System.out.println(studentList.get(0).getName());

        /*Student student = studentRepository.findById(id);
        System.out.println(student.getName());*/
        studentList = studentRepository.findByIdList(idList);
        System.out.println(studentList.get(0).getName());
        System.out.println(studentList.get(1).getName());

        return studentList;
        //studentList.add(s);
    }
}
