package com.kgk.kafkastreamdemo.valueMappers;

import com.kgk.kafkastreamdemo.entity.Student;
import com.kgk.kafkastreamdemo.service.StudentService;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class StudentValueMapper implements ValueMapper<List<Integer>, Student> {

    private static final Logger logger = LoggerFactory.getLogger(StudentValueMapper.class);
    private StudentService studentService;

    public StudentValueMapper(StudentService studentService){
        this.studentService = studentService;
    }

/*    @Override
    public Student apply(StudentKafkaRecordKey value) {
        logger.info("StudentKafkaRecordKey value = ", value);

        Student student = new Student();
        student.setId(1);
        return student;
    }*/

    @Override
    public Student apply(List<Integer> value) {
        List<Student> studentList = studentService.getStudentList(value);
        logger.info(studentList.get(0).getName());
        return studentList.get(0);
    }
}
