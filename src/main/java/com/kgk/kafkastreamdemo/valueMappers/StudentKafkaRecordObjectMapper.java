package com.kgk.kafkastreamdemo.valueMappers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kgk.kafkastreamdemo.dto.StudentKafkaRecordKey;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StudentKafkaRecordObjectMapper implements ValueMapper<String, StudentKafkaRecordKey> {
    private static final Logger logger = LoggerFactory.getLogger(StudentKafkaRecordObjectMapper.class);
    ObjectMapper objectMapper = new ObjectMapper();

        public StudentKafkaRecordObjectMapper(){
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Override
    public StudentKafkaRecordKey apply(String value) {
        logger.info("3. StudentKafkaRecordKey()");

        try {
            return objectMapper.readValue(value, StudentKafkaRecordKey.class);

        } catch (Exception e){
            logger.error("4. Error : {} ", e.getMessage());
            return null;
        }
    }
}
