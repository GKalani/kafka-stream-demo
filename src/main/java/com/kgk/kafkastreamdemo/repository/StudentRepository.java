package com.kgk.kafkastreamdemo.repository;

import com.kgk.kafkastreamdemo.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("select s from Student s where s.id = :id")
    Student findById(@Param("id") int id);

    @Query("SELECT t FROM Student t")
    List<Student> findAll();

    @Query("select s from Student s where s.id in (:idList)")
    List<Student> findByIdList(@Param("idList") List<Integer> idList);

}
