package com.kgk.kafkastreamdemo.configs;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.config.KafkaStreamsConfiguration;

import java.util.HashMap;
import java.util.Map;

import static org.apache.kafka.streams.StreamsConfig.APPLICATION_ID_CONFIG;

@Configuration
@EnableKafka
@EnableKafkaStreams
public class KafkaConfigs {

    @Value(value = "${applicationId}")
    private String applicationId;

    @Value(value = "${kafka.bootstrap.servers}")
    private String bootstrapServer;

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    KafkaStreamsConfiguration kStreamsConfig() {
        Map<String, Object> streamConfigs = new HashMap<>();
        streamConfigs.put(APPLICATION_ID_CONFIG, applicationId);
        streamConfigs.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        streamConfigs.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        streamConfigs.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,Serdes.String().getClass());
       // streamConfigs.put(StreamsConfig.STATE_DIR_CONFIG, Files.createTempDirectory("kafka-streams").toAbsolutePath().toString());
        return new KafkaStreamsConfiguration(streamConfigs);
    }

/*    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    KafkaStreamsConfiguration kStreamsConfig() {
        Map<String, Object> props = new HashMap<>();
        props.put(APPLICATION_ID_CONFIG, streamAppId);
        props.put(BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
//        props.put(ROCKSDB_CONFIG_SETTER_CLASS_CONFIG, CustomRocksDBConfig.class );

        props.put("security.protocol", "SSL");
//
        props.put("ssl.keystore.location", "/opt/keystores/trax-non-prod.keystore.jks");
//        props.put("ssl.keystore.location", "/Users/cmbklap0132/Documents/Shipx/MSK/jks_files/non-prod-kafka.client.keystore.jks");
        props.put("ssl.keystore.password", "test@123");
        props.put("enable.idempotence", false);


        return new KafkaStreamsConfiguration(props);
    }*/


}
